---
layout: post
title: About
permalink: /about/
show_title: false
---

Just a personal blog where I will post about anything that passes through my mind
from programming, to tech things that happen in my everyday. Feel free to follow 
the feed or check my social media, only if you want :)
