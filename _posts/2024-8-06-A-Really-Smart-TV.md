---
layout: post
title: A really "Smart" TV
---

Everyone loves to watch some TV from time to time, right?

Over the decades TVs have evolved a lot, but sometimes the human nature behind
it shows us how "Smart" TVs are not sometimes that smart.

When you write a program which makes a connection to the outside world, 
you might be thinking:

-- Hey, the connection can always fail by a lot of ways out of my control. How
could I handle this appropiately?

Then **somehow** you come to this conclusion:

-- Oh, I know. Lets do retry the process ad-infinitum without neither a retry 
count or a wait time.

Well, if you came to that conclusion, you might be elegible for working for
Netflix or whoever's app on LG TVs uses alphonso.tv:

```
# Think of this lines repeated a hundred times per minute on logs
query[A] logs.netflix.com from 192.168.1.xxx
query[AAAA] logs.netflix.com from 192.168.1.xxx
reply logs.netflix.com is NXDOMAIN
reply logs.netflix.com is NXDOMAIN
query[A] cdn-aas-ssai.alphonso.tv from dead:beef:dead:beef:dead:beef:dead:beef
query[AAAA] cdn-aas-ssai.alphonso.tv from dead:beef:dead:beef:dead:beef:dead:beef
reply cdn-aas-ssai.alphonso.tv is NXDOMAIN
reply cdn-aas-ssai.alphonso.tv is NXDOMAIN
```

This is very inconsiderate, specially towards

- Myself, the end user
- My home network (especially on Wi-Fi where bandwith is not always enough)
- My Internet and/or DNS providers (DNS might not be a network consuming beast 
for the average user, but bandwith and processing power are not free, specially
when you are on a economy of scale business).

Fortunately, I know that a lot of systems have
a reasonable TCP timeout time (or at least more reasonable than whoever wrote 
this DNS resolving code), so I made this simple dnsmasq rule which makes the 
devices hang on itselfs trying to connect to `0.0.0.0`/`::`.

```
/cdn-aas-ssai.alphonso.tv/#
/logs.netflix.com/#
```

And now, at least they don't spam DNS queries...

*Update*: After posting it I discovered that Alphonso was acquired in 2021 by LG
and rebranded to LG Ad Solutions.
