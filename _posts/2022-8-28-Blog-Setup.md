---
layout: post
title: Blog Setup
---

This is my first blog post. I am not 100% sure yet how I'm gonna handle this but 
I suppose that it's part of the fun involved in doing it. So, for this first blog post 
I will just talk about my process of creating this blog.

# Choosing a platform or technology. 

My first thoughts were to build something simple which didn't require much 
to get up and running (no database, no hosting required). Also, something I had in my 
mind was having an RSS/Atom feed. This is kinda personal thinking, but I don't like 
opening a lot of websites to get my news/info, and I prefer to follow open standards.

First, I was thinking about using Blogger, but I ended up using GitHub Pages with Jekyll.
As someone with development experience, I like to use tools which are flexible for someone 
like me with enough skills to use it. Blogger has a more user-friendly interface, but I 
prefer some freedom to do some tweaks if they are necessary.

# Why Jekyll?

Jekyll is a very simple approach to making a simple blog like this.
The config is in YAML format, the website pages are made in Markdown and jekyll simply generates 
a static html from predefined themes. Additionally, it supports some basic templating.
Creating a post is as simple as creating a new file in `_post` and uploading the changes.

A simple solution for a simple static website.
