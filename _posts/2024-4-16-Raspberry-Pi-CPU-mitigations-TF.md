---
layout: post
title: Raspberry Pi, CPU mitigations and TF
mermaid: true
excerpt: ""
---

For a little of context, I have a Raspberry Pi 4B, I got it when i wanted to start tinkering with Linux and aarch64 some time ago.
The boot flow of the Raspberry Pi for booting upstream software tends to look like this:

```mermaid
graph TD;
accTitle: The boot process of a Raspberry Pi using U-Boot and GRUB2;
  A["RPi Firmware<br>Stored in EEPROM flash<br>(1st stage Bootloader)"]--Custom<br>protocol -->B;
  B["Das U-Boot<br>Stored in SD/USB storage<br>(2nd stage Bootloader)"]--UEFI-->C;
  C["GRUB2<br>(3rd stage Bootloader)"]-->D;
  D[Linux kernel];
```

In this post I will almost exclusively talk about the 1st stage bootloader, which is configurable through a [config.txt][0] file.

## The problem
Recently I was checking the cpu features when I stumbled across this:

```
[c512l@rpi4 ~]$ lscpu
  ...
  Spec store bypass:      Vulnerable
  Mitigation;             __user pointer sanitization
  Spectre v2:             Vulnerable
  ...
```
Okay...? There is some reason why CPU bug mitigations are not completely applied...

## More problems
The first relevant thing I find on is this issue: [raspberrypi/tools\#114][1]
![Open issue for 4 years](../assets/posts/pi4-spectrev4-issue.webp)
Which also leaded me to unmerged PRs [#115][2] and [#121][2], patching it. After compiling it myself and trying a few configurations everything worked "fine". Which leaded to few questions emerging on my head:

Why is this unmerged?  

Why latest EEPROM firmware doesn't patch this?  

What is the reasoning behind deprecating the [armstub][4] option if this is still needed?  

Is there any cleaner solution to this?

Well.. I found the answer to the last one at least.

## A more standardized solution
Introducing, [ARM Trusted Firmware][5]:

- Developed by ARM technologies.
- Implements HW mitigations and a series of interfaces
- Completely replaces the mostly unmaintained raspberry armstub (although still loading it through the `armstub` option)
- Much more mature and battle tested

Sounds perfect isn't it? Well, kinda, there are still a [few caveats][6] but hey, it's better than a kick on the teeth.  

## Some tailoring
The only remaining problem is having to automate it was a pain point to do. Fortunately for me, Fedora ships a [arm-trusted-firmware-armv8][7] package, to which they enabled building binaries for the `rpi4` platform on the F40 development cycle, which is expected to be released this month. For now, I resort to using the rpm, extracting the binary and adding the configuration.

And here the final result:

```
[c512l@rpi4 ~]$ lscpu
  ...
  Spec store bypass:      Not affected
  Spectre v1:             Mitigation; __user pointer sanitization
  Spectre v2:             Mitigation; Branch predictor hardening, BHB
  ...
```

Conclusions? None, make you own, and let me know through any of my links! I'm considering adding a comment section using something like Disqus but I would prefer something not that privacy invasive and without much setup (something on the likes [giscus][8] or [utterances][9]).

[0]:https://www.raspberrypi.com/documentation/computers/config_txt.html
[1]:https://www.github.com/raspberrypi/firmware/issues/1451
[2]:https://www.github.com/raspberrypi/tools/pull/115
[3]:https://www.github.com/raspberrypi/tools/pull/121
[4]:https://www.raspberrypi.com/documentation/computers/legacy_config_txt.html#armstub
[5]:https://www.github.com/ARM-software/arm-trusted-firmware
[6]:https://www.github.com/ARM-software/arm-trusted-firmware/blob/master/docs/plat/rpi4.rst
[7]:https://src.fedoraproject.org/rpms/arm-trusted-firmware
[8]:https://giscus.app/
[9]:https://utteranc.es/
