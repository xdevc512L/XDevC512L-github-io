---
layout: post
title: Autoconfig Hell
---
Because CNAME is not enough (unfortunately)

Recently I started setting up my own domain for email. One of the things I noticed was that mail clients didn't find the IMAP/SMTP configs automatically. This is usually handled differently by each email client, and there is not a common discovery standard which everyone agrees on. The [evolution-docs] are a great resource which helped me a lot to understand how clients lookup for configuration.

With [RFC6186], since SRV records point to a domain, it works well if using external domains (i.e: an external mail provider who already hosts its config which you just want to "pass" from your own domain)

```
_autodiscover._tcp.c512l.dev. 3600 IN SRV 0 0 443 mailbox.org.
```

On the other hand, [draft-autoconfig-1] is a bit more problematic. You need to have `autoconfig.%EMAILDOMAIN%` resolving to something to which a HTTP request will be made. You can "use" a CNAME record, but that doesn't rewrite your `Host` header in the HTTP Headers (because it's not a redirection), and almost every connection this days uses HTTPS.

You can guess what's coming right?

`SSL_ERROR_BAD_CERT_DOMAIN`

Yeah...

Coincidentally, K-9 Mail (or Thunderbird Mobile in the future), is one of those who don't support RFC6186 [yet]...

# GitLab Pages at the rescue!
Normally this would mean preparing a VPS or a dedicated server and setting up a web server to redirect the request. Fortunately, GitLab Pages provides options for configuring server-side [redirection rules]. So I just set up a basic single file page with a `_redirects` file, redirecting to the correct server. You can check the [repo] on my gitlab. And maybe find "something else"

# Conclusion

![K-9 Mail Success]

# **NICE**

[evolution-docs]:https://gitlab.gnome.org/GNOME/evolution/-/wikis/Autoconfig
[RFC6186]:https://www.rfc-editor.org/rfc/rfc6186
[redirection rules]:https://docs.gitlab.com/ee/user/project/pages/redirects.html
[draft-autoconfig-1]:https://benbucksch.github.io/autoconfig-spec/draft-autoconfig-1.html
[yet]:https://github.com/thunderbird/thunderbird-android/issues/4721
[repo]:https://gitlab.com/c512l/mail-autoconfig-redirect
{:width="500px"}
[K-9 Mail Success]:/assets/posts/k9-discovery-success.webp
