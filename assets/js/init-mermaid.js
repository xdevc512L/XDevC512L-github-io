// SPDX-FileCopyrightText: 2024 Vicente Maroto Garzón <vicente.maroto.garzon@c512l.dev>
//
// SPDX-License-Identifier: GPL-3.0-only
"use strict";

if (document.readyState !== 'loading') {
  initMermaid();
} else {
  document.addEventListener('DOMContentLoaded', initMermaid);
}
function initMermaid() {
  mermaid.initialize({
    theme: 'dark',
    securityLevel: 'antiscript'
  });
}
