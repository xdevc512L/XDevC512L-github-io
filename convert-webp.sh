#!/bin/bash

# SPDX-FileCopyrightText: 2024 Vicente Maroto Garzón <vicente.maroto.garzon@c512l.dev>
#
# SPDX-License-Identifier: GPL-3.0-only

set -e

search_paths=(
  'assets/posts'
  'assets/img'
)

optim_path() {
  for file in "$@"; do
    echo "Optimizing $file"
    output="${file%.*}.webp"
    cwebp "$file" -o "$output"
    rm -f "$file"
  done
}

find_and_optimize() {
  for path in "${search_paths[@]}"; do
    mapfile -t e < <(find "$path" ! -name '*.webp' ! -name '*.svg' -type f)
    optim_path "${e[@]}"
  done
}

find_and_optimize
#mapfile -t post_unop_files < <(find $POST_ASSETS_PATH ! -name '*.webp' -type f)
#mapfile -t img_unop_files < <(find $IMG_ASSETS_PATH ! -name '*.webp' -type f)
