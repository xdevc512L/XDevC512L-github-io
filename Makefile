# SPDX-FileCopyrightText: 2024 Vicente Maroto Garzón <vicente.maroto.garzon@c512l.dev>
# SPDX-License-Identifier: CC0-1.0

BUNDLE := bundle
REUSE := reuse
RM := rm

OUT_DIR := _site

SERVE_PORT := 80
SERVE_HOST := localhost

SERVE_ARGS := \
		-H $(SERVE_HOST) \
		-P $(SERVE_PORT) \
		-d $(OUT_DIR) \
		--watch \
		-o

BUILD_ARGS := \
		-V \
		-d $(OUT_DIR)

.PHONY: install build serve reuse gzip clean

lint:
		$(REUSE) lint
install:
		$(BUNDLE) install
build: install
		$(BUNDLE) exec jekyll build $(BUILD_ARGS)
gzip:
		find $(OUT_DIR) \( -name '*.html' -o -name '*.css' -o -name '*.js' \) -print0 | xargs -0 gzip -9 -kv
serve:
		$(BUNDLE) exec jekyll serve $(SERVE_ARGS)
clean:
	@-$(RM) -rf $(OUT_DIR)
